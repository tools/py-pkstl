from pkstl.libpkstl import *

__all__ = ["__title__", "__summary__", "__uri__", "__version__", "__author__", "__email__", "__license__", "__copyright__"]

__author__ = "elois <c@elo.tf>, Pascal Engélibert <tuxmain@zettascript.org>"
__copyright__ = "Copyright 2019-2020 {0}".format(__author__)
__license__ = "AGPL 3.0"
__summary__ = "Public key secure transport layer"
__title__ = "PKSTL"
__uri__ = "https://git.duniter.org/tools/py-pkstl"
__version__ = "0.1.0"

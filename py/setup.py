import setuptools
from setuptools.dist import Distribution

class BinaryDistribution(Distribution):
	def is_pure(self):
		return False

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
	name="pkstl",
	version="0.1.0",
	author="Pascal Engélibert",
	author_email="tuxmain@zettascript.org",
	description="Public key secure transport layer",
	long_description=long_description,
	long_description_content_type="text/markdown",
	url="https://git.duniter.org/tools/py-pkstl",
	packages=["pkstl"],
	package_dir={"": "src"},
	ext_package="pkstl",
	package_data={"":["*.so"]},
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: GNU Affero General Public License v3",
		"Operating System :: OS Independent",
		"Topic :: Security :: Cryptography"
	],
	keywords="security network transport cryptography",
	python_requires='>=3.6',
	include_package_data=True,
	distclass=BinaryDistribution,
)

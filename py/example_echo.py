#!/usr/bin/env python3

import sys, asyncio, io, pkstl, websockets

HOST = ("127.0.0.1", 56123)

MSG_CONNECT = 1
MSG_ACK = 2
MSG_MESSAGE = 3

async def send_rec_req(host, data, path=""):
	async with websockets.connect("ws://{}:{}{}".format(*host, path)) as websocket:
		await websocket.send(data)
		return await websocket.recv()

async def server_handler(websocket, path, seed):
	if path == "/pubkey":
		await websocket.send(pkstl.Ed25519KeyPair.from_seed_unchecked(seed.bytes).pubkey)
		return
	
	msl = pkstl.SecureLayer.create(pkstl.SecureLayerConfig.default(), seed)
	client_pubkey = None
	client_ack = False
	
	try:
		async for data in websocket:
			msgs_in = msl.read_bin(data)
			for msg_in in msgs_in:
				if msg_in.msg_type == MSG_CONNECT and client_pubkey == None:
					client_pubkey = msg_in.data[1]
					print("rec connect")
					msg_out = msl.write_connect_msg_bin(b"Hello client!")
					await websocket.send(msg_out)
					msg_out = msl.write_ack_msg_bin(b"Hello client again!")
					await websocket.send(msg_out)
				elif msg_in.msg_type == MSG_ACK and not client_ack:
					client_ack = True
					print("rec ack")
				elif msg_in.msg_type == MSG_MESSAGE:
					print("rec msg")
					msg_out = msl.write_bin(b"You said `"+msg_in.data[0]+b"`!")
					await websocket.send(msg_out)
	except websockets.exceptions.ConnectionClosedError:
		print("Connection unexpectedly closed by client")

def main_server(host):
	seed = pkstl.Seed32.random()
	pubkey = pkstl.Ed25519KeyPair.from_seed_unchecked(seed.bytes).pubkey
	
	async def _server_handler(websocket, path):
		return await server_handler(websocket, path, seed)
	
	sock = websockets.serve(_server_handler, *HOST)
	
	asyncio.get_event_loop().run_until_complete(sock)
	asyncio.get_event_loop().run_forever()

async def main_client(host):
	server_pubkey = await send_rec_req(host, b"", "/pubkey")
	
	seed = pkstl.Seed32.random()
	msl = pkstl.SecureLayer.create(pkstl.SecureLayerConfig.default(), seed, server_pubkey)
	pubkey = pkstl.Ed25519KeyPair.from_seed_unchecked(seed.bytes).pubkey
	
	async with websockets.connect("ws://{}:{}".format(*host)) as websocket:
		
		# send CONNECT
		msg_out = msl.write_connect_msg_bin(b"Hello world!")
		await websocket.send(msg_out)
		
		# receive CONNECT
		ok = False
		loop = True
		while loop:
			res = await websocket.recv()
			msgs_in = msl.read_bin(res)
			for msg in msgs_in:
				if not ok and msg.msg_type == MSG_CONNECT:
					ok = True
					print("rec connect")
					# send ACK
					msg_out = msl.write_ack_msg_bin(b"Client ACK!")
					await websocket.send(msg_out)
				elif ok and msg.msg_type == MSG_ACK:
					print("rec ack")
					loop = False
					break
		
		print("Type a message to the server. `q` to quit.")
		while True:
			if (msg := input(">")) == "q":
				break
			msg_out = msl.write_bin(msg.encode())
			await websocket.send(msg_out)
			res = await websocket.recv()
			msgs_in = msl.read_bin(res)
			for msg_in in msgs_in:
				if msg_in.msg_type == MSG_MESSAGE:
					print("<", msg_in.data[0].decode())

def print_help():
	print("Usage: python3 example_echo.py <server|client>")
	exit()

if __name__ == "__main__":
	if len(sys.argv) > 1:
		if sys.argv[1] == "server":
			main_server(HOST)
		
		elif sys.argv[1] == "client":
			asyncio.run(main_client(HOST))
		
		else:
			print_help()
	else:
		print_help()

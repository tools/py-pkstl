cargo build --release

mkdir -p target/wheel/src/pkstl

cp py/setup.py target/wheel/
cp py/__init__.py target/wheel/src/pkstl/
cp py/__test__.py target/wheel/src/pkstl/
cp LICENSE target/wheel/
cp README.md target/wheel/
cp target/release/libpy_pkstl.so target/wheel/src/pkstl/libpkstl.so

cd target/wheel

python3 setup.py sdist bdist_wheel
